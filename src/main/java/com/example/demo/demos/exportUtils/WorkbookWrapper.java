package com.example.demo.demos.exportUtils;


import com.example.demo.demos.exportUtils.interfaceUtils.Writable;
import org.apache.poi.ss.usermodel.Workbook;

import java.io.IOException;
import java.io.OutputStream;

/**
 * @Author xlj
 * @Date Created in  2024/3/15 17:12
 * @DESCRIPTION:
 * @Version V1.0
 */

public class WorkbookWrapper implements Writable {
    private Workbook workbook;

    public WorkbookWrapper(Workbook workbook) {
        this.workbook = workbook;
    }

    @Override
    public void write(OutputStream outputStream) throws IOException {
        this.workbook.write(outputStream);
    }

}

package com.example.demo.demos.exportUtils;



import com.example.demo.demos.exportUtils.interfaceUtils.TemplateRenderer;
import com.example.demo.demos.exportUtils.interfaceUtils.Writable;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Map;

/**
 * @Author xlj
 * @Date Created in  2024/3/15 16:58
 * @DESCRIPTION:
 * @Version V1.0
 */

public abstract class AbsExporter implements TemplateRenderer {

    public void doExport(Map<String, Object> dataSource, File exportedFile) throws Throwable {
        try(FileOutputStream fos = new FileOutputStream(exportedFile)) {
            Writable writable = this.render(dataSource);
            writable.write(fos);
        }
    }

    public abstract String getTargetFileSuffix();

    public void afterExport() {}

}

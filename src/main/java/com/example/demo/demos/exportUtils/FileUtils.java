package com.example.demo.demos.exportUtils;

import org.apache.commons.lang3.StringUtils;

import java.io.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * @Author xlj
 * @Date Created in  2024/3/15 17:02
 * @DESCRIPTION:
 * @Version V1.0
 */

public class FileUtils {
    static {
        // 当文件系统中没有nhtemp文件夹的时候，创建
        File sf = new File(FileUtils.filePathJoin(System.getProperty("java.io.tmpdir"), "nhtemp"));
        if (!sf.exists()) {
            sf.mkdirs();
        }
    }

    /**
     * 临时文件夹，在临时文件夹中创建nhtemp，用来保存所有使用本工具类创建的文件，以便于统一清空临时文件夹,并且已经包含了文件分割符号，请注意
     */
    public static final String TEMP_FILE_PATH = FileUtils.filePathJoin(System.getProperty("java.io.tmpdir"), "nhtemp");

    /**
     * 向文件写入数据
     *
     * @param is
     * @param file
     * @throws IOException
     */
    public static void writeToFile(InputStream is, File file) throws IOException {
        FileOutputStream fs = null;
        try {
            fs = new FileOutputStream(file);
            byte[] buffer = new byte[1024];
            int byteread = 0;
            while ((byteread = is.read(buffer)) != -1) {
                fs.write(buffer, 0, byteread);
            }
        } catch (IOException e) {
            throw e;
        } finally {
            if (fs != null) {
                fs.close();
            }
            is.close();
        }
    }

    /**
     * 删除文件夹（会删除文件夹下所有的文件）
     *
     * @param dir
     * @return
     */
    public static boolean deleteDir(File dir) {
        if (dir.isDirectory()) {
            String[] children = dir.list();
            //递归删除目录中的子目录下
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
        }
        // 目录此时为空，可以删除
        return dir.delete();
    }

    public static File createDir(String dirPath) {
        File dir = new File(dirPath);
        //如果文件夹不存在
        if (!dir.exists()) {
            //创建文件夹
            dir.mkdir();
        }
        return dir;
    }

    public static void zipDir(String directoryName, ZipOutputStream zos, BufferedOutputStream bos) {
        File file = new File(directoryName);
        if (file.exists()) {
            File[] fileList = file.listFiles();
            assert fileList != null;
            for (File f : fileList) {
                // 压缩单个文件到 zos
                String zipName = f.getName();
                try {
                    zos.putNextEntry(new ZipEntry(zipName));
                    int len;
                    FileInputStream is = new FileInputStream(f);
                    BufferedInputStream bis = new BufferedInputStream(is);
                    byte[] bytes = new byte[1024];
                    while ((len = bis.read(bytes)) != -1) {
                        bos.write(bytes, 0, len);

                    }
                    bos.flush();
                    zos.flush();

//                    结束当前压缩文件的添加
                    bis.close();
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();

                }

            }

        }
    }

    /**
     * 路径拼接工具方法
     * @param filePath 文件路径
     * @return 拼接结果
     */
    public static String filePathJoin(String... filePath) {
        return StringUtils.join(filePath, File.separator);
    }

}

package com.example.demo.demos.exportUtils.interfaceUtils;

import java.io.IOException;
import java.io.OutputStream;

/**
 * @Author xlj
 * @Date Created in  2024/3/15 16:57
 * @DESCRIPTION:
 * @Version V1.0
 */

public interface Writable {
    void write(OutputStream outputStream) throws IOException;
}

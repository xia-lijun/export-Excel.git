package com.example.demo.demos.exportUtils;

import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.apache.velocity.exception.VelocityException;

import java.io.StringWriter;
import java.util.Map;
import java.util.Properties;

/**
 * @Author xlj
 * @Date Created in  2024/3/15 17:09
 * @DESCRIPTION:
 * @Version V1.0
 */

public class VelocityUtils {
    /**
     * 模板文件所在目录
     */
    private static final String TEMPLATE_FILE_DIR = FileUtils.filePathJoin("file", "velocityTemp");

    static {
        //初始化参数
        Properties properties = new Properties();
        //设置 velocity 资源加载方式为 class
        properties.setProperty("resource.loader", "class");
        //设置 velocity 资源加载方式为 class 时的处理类
        properties.setProperty("class.resource.loader.class", "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
        // 执行初始化
        Velocity.init(properties);
    }

    /**
     * 渲染对应模板，并输出渲染结果
     * @param templateFileName 模板文件名
     * @param velocityContext 上下文对象，即渲染使用的数据源
     * @return 渲染结果
     */
    public static String render(String templateFileName, VelocityContext velocityContext) throws VelocityException {
        StringWriter writer = new StringWriter();
        Velocity.mergeTemplate(FileUtils.filePathJoin(TEMPLATE_FILE_DIR, templateFileName), "UTF-8", velocityContext, writer);
        return writer.toString();
    }

    /**
     * 渲染对应模板，并输出渲染结果
     * @param templateFileName 模板文件名
     * @param renderDataSource 渲染使用的数据源
     * @return 渲染结果
     */
    public static String render(String templateFileName, Map<String, Object> renderDataSource) throws VelocityException {
        VelocityContext velocityContext = new VelocityContext(renderDataSource);
        return render(templateFileName, velocityContext);
    }

}

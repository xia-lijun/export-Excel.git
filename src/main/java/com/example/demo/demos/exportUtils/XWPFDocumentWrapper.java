package com.example.demo.demos.exportUtils;


import com.example.demo.demos.exportUtils.interfaceUtils.Writable;
import org.apache.poi.xwpf.usermodel.XWPFDocument;

import java.io.IOException;
import java.io.OutputStream;


/**
 * @Author xlj
 * @Date Created in  2024/3/15 17:14
 * @DESCRIPTION:
 * @Version V1.0
 */

public class XWPFDocumentWrapper implements Writable {
    private XWPFDocument xwpfDocument;

    public XWPFDocumentWrapper(XWPFDocument xwpfDocument) {
        this.xwpfDocument = xwpfDocument;
    }

    @Override
    public void write(OutputStream outputStream) throws IOException {
        this.xwpfDocument.write(outputStream);
    }

}

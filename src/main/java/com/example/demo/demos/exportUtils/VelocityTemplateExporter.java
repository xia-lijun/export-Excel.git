package com.example.demo.demos.exportUtils;

import cn.afterturn.easypoi.excel.ExcelXorHtmlUtil;
import cn.afterturn.easypoi.excel.entity.enmus.ExcelType;
import com.example.demo.demos.exportUtils.interfaceUtils.Writable;
import org.apache.poi.ss.usermodel.Workbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

/**
 * @Author xlj
 * @Date Created in  2024/3/15 17:08
 * @DESCRIPTION:
 * @Version V1.0
 */

public class VelocityTemplateExporter extends AbsExporter{
    private static final Logger LOGGER = LoggerFactory.getLogger(VelocityTemplateExporter.class);

    private String templateFilename;

    public VelocityTemplateExporter(String templateFilename) {
        this.templateFilename = templateFilename;
    }

    @Override
    public String getTargetFileSuffix() {
        return ".xlsx";
    }

    @Override
    public Writable render(Map<String, Object> dataSource) {
        String html = VelocityUtils.render(this.templateFilename + ".vm", dataSource);
        LOGGER.trace("渲染的html为：\n{}", html);
        Workbook workbook = ExcelXorHtmlUtil.htmlToExcel(html, ExcelType.XSSF);
        if (null == workbook) {
            throw new NullPointerException("workbook 为 null");
        }
        return new WorkbookWrapper(workbook);
    }

}

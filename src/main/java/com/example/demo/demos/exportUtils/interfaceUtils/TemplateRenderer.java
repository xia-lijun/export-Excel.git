package com.example.demo.demos.exportUtils.interfaceUtils;

import java.util.Map;

/**
 * @Author xlj
 * @Date Created in  2024/3/15 16:54
 * @DESCRIPTION:
 * @Version V1.0
 */

public interface TemplateRenderer {
    Writable render(Map<String, Object> dataSource) throws Throwable;
}

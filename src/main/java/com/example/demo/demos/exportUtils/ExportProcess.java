package com.example.demo.demos.exportUtils;


import com.example.demo.demos.exportUtils.interfaceUtils.ExportedFileNameFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.zip.ZipOutputStream;

/**
 * @Author xlj
 * @Date Created in  2024/3/15 17:00
 * @DESCRIPTION:
 * @Version V1.0
 */

public class ExportProcess {
    private static final Logger LOGGER = LoggerFactory.getLogger(ExportProcess.class);

    /**
     * 要导出的数据源
     */
    private List<Map<String, Object>> dataSourceList = new ArrayList<>();

    /**
     * 是否为多文件导出
     */
    private boolean multiFile;

    /**
     * 导出器
     */
    private AbsExporter exporter;

    /**
     * 导出文件名
     */
    private String exportedFilename;

    /**
     * 导出为多文件时的文件名命名工厂
     */
    private ExportedFileNameFactory nameFactory;

    private ExportProcess(Map<String, Object> dataSource, AbsExporter exporter, String exportedFilename) {
        this.dataSourceList.add(dataSource);
        this.multiFile = false;
        this.exporter = exporter;
        this.exportedFilename = exportedFilename;
    }

    private ExportProcess(List<Map<String, Object>> dataSourceList, AbsExporter exporter, String exportedFilename, ExportedFileNameFactory nameFactory) {
        this.dataSourceList.addAll(dataSourceList);
        this.multiFile = true;
        this.exporter = exporter;
        this.exportedFilename = exportedFilename;
        this.nameFactory = nameFactory;
    }

    public static ExportProcess newProcess(Map<String, Object> dataSource, AbsExporter exporter, String exportedFilename) {
        return new ExportProcess(dataSource, exporter, exportedFilename);
    }

    public static ExportProcess newProcess(List<Map<String, Object>> dataSourceList, AbsExporter exporter, String exportedFilename, ExportedFileNameFactory nameFactory) {
        return new ExportProcess(dataSourceList, exporter, exportedFilename, nameFactory);
    }

    public ExportResult export() {
        ExportResult exportResult = new ExportResult(this.multiFile ? exportAsZipFile() : exportAsSingleFile());
        this.exporter.afterExport();
        return exportResult;
    }

    /**
     * 导出为单文件
     * @return 导出结果
     */
    private File exportAsSingleFile() {
        Map<String, Object> dataSource = this.dataSourceList.get(0);
        // 导出文件所在目录路径
        String exportedFileDirPath = FileUtils.filePathJoin(FileUtils.TEMP_FILE_PATH, "exportedFileDir" + UUID.randomUUID().toString());
        // 创建导出文件所在目录
        File exportedFileDir = FileUtils.createDir(exportedFileDirPath);
        String exportedFilePath = FileUtils.filePathJoin(exportedFileDirPath, this.exportedFilename + this.exporter.getTargetFileSuffix());
        File exportedFile = new File(exportedFilePath);
        try {
            this.exporter.doExport(dataSource, exportedFile);
            return exportedFile;
        } catch (Throwable t) {
            LOGGER.error(t.getMessage(), t);
            FileUtils.deleteDir(exportedFileDir);
        }
        return null;
    }

    /**
     * 导出为压缩文件
     * @return 导出结果
     */
    private File exportAsZipFile() {
        String tempFileDirPath = FileUtils.filePathJoin(FileUtils.TEMP_FILE_PATH, "tempFile" + UUID.randomUUID().toString());
        File tempFileDir = FileUtils.createDir(tempFileDirPath);
        // 导出文件所在目录路径
        String exportedFileDirPath = FileUtils.filePathJoin(FileUtils.TEMP_FILE_PATH, "exportedFileDir" + UUID.randomUUID().toString());
        // 创建导出文件所在目录
        File exportedFileDir = FileUtils.createDir(exportedFileDirPath);
        File exportedFile = new File(FileUtils.filePathJoin(exportedFileDirPath, this.exportedFilename + ".zip"));
        try {
            for (Map<String, Object> dataSource : this.dataSourceList) {
                this.exporter.doExport(dataSource, new File(FileUtils.filePathJoin(tempFileDirPath, this.nameFactory.getName(dataSource) + this.exporter.getTargetFileSuffix())));
            }
            try (ZipOutputStream out = new ZipOutputStream(new FileOutputStream(exportedFile));
                 BufferedOutputStream bos = new BufferedOutputStream(out)) {
                FileUtils.zipDir(tempFileDirPath, out, bos);
            }
            return exportedFile;
        } catch (Throwable t) {
            LOGGER.error(t.getMessage(), t);
            FileUtils.deleteDir(exportedFileDir);
        } finally {
            FileUtils.deleteDir(tempFileDir);
        }
        return null;
    }
}

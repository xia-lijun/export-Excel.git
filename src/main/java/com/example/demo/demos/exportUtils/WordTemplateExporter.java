package com.example.demo.demos.exportUtils;

import cn.afterturn.easypoi.word.WordExportUtil;
import com.example.demo.demos.exportUtils.interfaceUtils.Writable;
import org.apache.poi.xwpf.usermodel.XWPFDocument;

import java.util.Map;

/**
 * @Author xlj
 * @Date Created in  2024/3/15 17:16
 * @DESCRIPTION:
 * @Version V1.0
 */

public class WordTemplateExporter extends AbsExporter{
    private String templateFilePath;

    public WordTemplateExporter(String templateFilename) {
        this.templateFilePath = "file/excelTemp/" + templateFilename + ".docx";
    }

    @Override
    public String getTargetFileSuffix() {
        return ".docx";
    }

    @Override
    public Writable render(Map<String, Object> dataSource) throws Exception {
        XWPFDocument xwpfDocument = WordExportUtil.exportWord07(templateFilePath, dataSource);
        XWPFDocumentWrapper xwpfDocumentWrapper = new XWPFDocumentWrapper(xwpfDocument);
        return xwpfDocumentWrapper;
    }

}

package com.example.demo.demos.controller;


import com.example.demo.demos.exportUtils.ExcelTemplateExporter;
import com.example.demo.demos.exportUtils.ExportProcess;
import com.example.demo.demos.exportUtils.VelocityTemplateExporter;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author xlj
 * @Date Created in  2024/3/15 17:22
 * @DESCRIPTION:
 * @Version V1.0
 */
@RestController
public class ExportController {

    @GetMapping("/test")
    public void exportDownload(HttpServletRequest request, HttpServletResponse response){
        List<Map<String,Object>> list = new ArrayList<>();
        Map<String, Object> map1 = new HashMap<>();
        Map<String, Object> map2 = new HashMap<>();
        Map<String, Object> map3 = new HashMap<>();
        map1.put("yf","1月");
        map1.put("xm","100");
        map1.put("xb","99");
        map1.put("age","33");
        map1.put("yx","33");
        map1.put("bj","33");
        map1.put("qs","33");
        map1.put("cs","33");

        map2.put("yf","2月");
        map2.put("xm","100000");
        map2.put("xb","200000");
        map2.put("age","300000");
        map2.put("yx","300000");
        map2.put("bj","300000");
        map2.put("qs","300000");
        map2.put("cs","300000");

        map3.put("yf","3月");
        map3.put("xm","1");
        map3.put("xb","2");
        map3.put("age","3");
        map3.put("yx","4");
        map3.put("bj","5");
        map3.put("qs","6");
        map3.put("cs","7");

        list.add(map1);
        list.add(map2);
        list.add(map3);
        Map<String, Object> map = new HashMap<>();
        map.put("maplist",list);
        ExportProcess.newProcess(map,new ExcelTemplateExporter("2024第一污水处理厂再生水回用年报"),
                "2024第一污水处理厂再生水回用年报").export().download(request,response);
//        ExportProcess.newProcess(map,new VelocityTemplateExporter("2024第一污水处理厂再生水回用年报"),
//                "2024第一污水处理厂再生水回用年报").export().download(request,response);
    }

    @GetMapping("/exportDownloadTwo")
    public void exportDownloadTwo(HttpServletRequest request, HttpServletResponse response){
        List<Map<String,Object>> list = new ArrayList<>();
        Map<String, Object> map1 = new HashMap<>();
        Map<String, Object> map2 = new HashMap<>();
        Map<String, Object> map3 = new HashMap<>();
        map1.put("xm","100");
        map1.put("xb","99");
        map1.put("age","33");

        map2.put("xm","100000");
        map2.put("xb","200000");
        map2.put("age","300000");

        map3.put("xm","1");
        map3.put("xb","2");
        map3.put("age","3");

        list.add(map1);
        list.add(map2);
        list.add(map3);
        Map<String, Object> map = new HashMap<>();
        map.put("maplist",list);
//        ExportProcess.newProcess(map,new ExcelTemplateExporter("2024第一污水处理厂再生水回用年报"),
//                "2024第一污水处理厂再生水回用年报").export().download(request,response);
        ExportProcess.newProcess(map,new ExcelTemplateExporter("学生"),
                "学生").export().download(request,response);
    }
    @GetMapping("/exportDownloadTTT")
    public void exportDownloadTTT(HttpServletRequest request, HttpServletResponse response){
        Map<String, Object> map = new HashMap<>();
        ExportProcess.newProcess(map,new ExcelTemplateExporter("2024第一污水处理厂再生水回用年报"),
                "2024第一污水处理厂再生水回用年报").export().download(request,response);
//        ExportProcess.newProcess(map1,new VelocityTemplateExporter("2024第一污水处理厂再生水回用年报"),
//                "2024第一污水处理厂再生水回用年报").export().download(request,response);
    }
}
